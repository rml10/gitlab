  // DOM element where the Timeline will be attached
  var container = document.getElementById('timeline_forges');

  // Create a DataSet (allows two way data-binding)
  var items = new vis.DataSet([
    {id: 1, content: 'Libre - FusionForge', start: '2009'},
    {id: 2, content: 'Libre / Propriétaire - SourceForge', start: '1999'},
    {id: 3, content: 'Propriétaire - Github', start: '2008'},
    {id: 4, content: 'Libre / Propriétaire - Gitlab', start: '2011'},
    {id: 5, content: 'Libre / Propriétaire - Bitbucket', start: '2008'},
    {id: 6, content: 'Libre - Gitorious', start: '2008'},
    {id: 7, content: 'Libre - Launchpad', start: '2005'},
    {id: 8, content: 'Libre - Redmine', start: '2006'},
    {id: 9, content: 'Proprietary - Codeplex', start: '2006'}
  ]);

  // Configuration for the Timeline
  var options = {
    height: '800px'
};

  // Create a Timeline
  var timeline = new vis.Timeline(container, items, options);
