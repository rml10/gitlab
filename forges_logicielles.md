# Cahier des charges de notre forge logicielle

## La forge logicielle devrait inclure :
- Système de gestion de versions
- Système de gestion de forks et de merge requests
- Outil de suivi de tickets (bugs, évolutions, tâches)
- Gestionnaire de documentation (wiki)

> Le but étant de simplifier les interactions entre les différents membres de l'équipe au service de la montée en compétence et de la simplification de la communication entre les différents développeurs

## En besoin secondaire, elle devrait permettre de :
- Créer des builds sécurisés et fiables des logiciels du projet
- Générer des déploiements en production de certains sous-projets (site web)



# Forges logicielles - Leaders du marché

- __Github__
  - SaaS / Cloud
  - Pricing
    - Gratuit - Projets publics et opensource
    - 9$/user/mois - Projets publics et privés
    - 21$/user/mois - Mode entreprise, synchronisation LDAP, SLA, possibilité d'hosting dédié


# Forges logicielles - Leaders du marché

- __Bitbucket__
  - SaaS ou On Premise
  - Pricing
    - Gratuit <5 users (SaaS) ou 10$ <10 users (OP) - Toutes les fonctionnalités
    - 2$/user/mois (SaaS) ou 1800$ <25 users(OP) - Toutes les fonctionnalités
    - 5$/user/month (SaaS) ou 16000$/an <500 users (OP) - Toutes les fonctionnalités
  - Open Source et soumis à licence gratuite pour les projets sous une licence approuvée par l'Open Source Initiative


# Forges logicielles - Leaders du marché

- __GitLab__
  - SaaS ou On Premise
  - Pricing
    - Gratuit (OP & SaaS) - Fonctionnalités petites équipes
    - 3,25$/user/mois (OP) 4$/user/mois (SaaS) - Fonctionnalités grosses équipes
    - 16,59$/user/mois (OP) 19$/user/mois (SaaS) - On Premise fonctionnalités grosses entreprises mondiales
  - Libre et opensource pour la version communautaire on premise
  - Edité et développé par la société GitLab Inc.



# Composants GitLab utilisables pour Duniter
- Gestion des droits et des utilisateurs
- Regroupement logique des dépots de projets par groupes
- Versionning git
- Gestion des issues (tickets, demandes d'évolutions, bugs)
  - Création automatique de MR et de branches
  - Tracking des discussions
- Gestion des forks et des merge requests (appelées pull requests chez GitHub)
  - Tracking des discussions
  - Processus humain d'approbation
- GitLab CI
- Registry Docker
- Wiki
- GitLab pages



# Première connexion au GitLab Duniter
> __https://git.duniter.org__

- Disponible sur internet.
- Première connexion à réaliser avec son compte GitHub si besoin de migrer un projet depuis GitHub
- Disponibilité en git+ssh au moyen de clés ssh à renseigner dans l'interface web

![](images/forges_logicielles-59a456d8.png)



# Création d'un projet
> Bouton "New Project"

## Choix 1 : Projet vide
![](images/forges_logicielles-bf7a2ab2.png)

- Créer un fichier Readme.md par l'interface web.

# Création d'un projet
> Bouton "New Project"

## Choix 2 : Projet importé de Github
![](images/forges_logicielles-19905821.png)



# Création d'un issue
> Dans le projet, section "issues", bouton "New issue"

![](images/forges_logicielles-e5d6ad74.png)


# Création d'un issue - Mise en place d'un template d'issues
- Par l'interface web créer un nouveau fichier.
- Le nommer ```.gitlab/issue_templates/bug.md``` et insérer le contenu suivant

```
# Issue issue template
- [ ] What
- [ ] How
- [ ] When
```

![](images/forges_logicielles-b30c885e.png)



# Création d'une branche et d'une merge request associée à partir d'un issue

> Dans l'issue, bouton "Create a merge request" option "Creates a merge request named after this issue, with source branch created from 'master'."

![](images/forges_logicielles-d8b405ac.png)



# Création d'un fork

> Aller sur le projet d'un voisin (où vous n'avez pas les droits d'écriture) et créez un Fork dans le groupe de projets à votre nom puis avec l'éditeur en ligne, faites une modification au projet.

![](images/forges_logicielles-9b2a0e45.png)


# Création d'un fork - Merge request
> Merge Requests, bouton "New merge request"

![](images/forges_logicielles-be8f5242.png)

La merge request s'ouvre dans le projet "target". Approuvez celle créée par votre voisin.


# Création d'un fork - Maintient du fork à jour avec le dépot original

```
# Ajout d'un nouveau remote "upstream"
florck@florck-macbook [/tmp/gitlab] % git remote add upstream git@git.duniter.org:rml10/gitlab.git                                                                                                                              - (21:18)
florck@florck-macbook [/tmp/gitlab] % git remote -v                                                                                                                                                                             - (21:22)
origin	git@git.duniter.org:florck/gitlab.git (fetch)
origin	git@git.duniter.org:florck/gitlab.git (push)
upstream	git@git.duniter.org:rml10/gitlab.git (fetch)
upstream	git@git.duniter.org:rml10/gitlab.git (push)
```

```
# Recuperation des infos distantes
florck@florck-macbook [/tmp/gitlab] % git fetch upstream                                                                                                                                                                        - (21:22)
remote: Counting objects: 4, done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 4 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (4/4), done.
From git.duniter.org:rml10/gitlab
 * [new branch]      1-test     -> upstream/1-test
 * [new branch]      master     -> upstream/master
```

```
# Synchronisation de la branche locale
florck@florck-macbook [/tmp/gitlab] % git checkout master                                                                                                                                                                       - (21:22)
Already on 'master'
Your branch is up-to-date with 'origin/master'.
florck@florck-macbook [/tmp/gitlab] % git merge upstream/master                                                                                                                                                                 - (21:22)
Already up-to-date!
Merge made by the 'recursive' strategy.
```



# Création d'une page web deployée avec gitlab pages
## Création de la page web
> `index.html`

```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Hello World</title>
  </head>
  <body>
    <h1>Hello World</h1>
  </body>
</html>
```


# Création d'une page web deployée avec gitlab pages
## Création du fichier d'intégration continue
> `.gitlab-ci.yml`

```
pages:
  stage: deploy
  environment:
    name: production
    url: http://$CI_PROJECT_NAMESPACE.pages.duniter.org/$CI_PROJECT_NAME
  script:
  - mkdir .public
  - cp -r index.html .public
  - mv .public public
  artifacts:
    paths:
    - public
  tags:
  - pages
  only:
  - master
```


# Création d'une page web deployée avec gitlab pages
## Activation du runner dédié aux pages
![](images/forges_logicielles-6810b627.png)


# Création d'une page web deployée avec gitlab pages
## Suivi de l'évolution de la pipeline
![](images/forges_logicielles-4330791c.png)


# Création d'une page web déployée avec gitlab pages
## Visite de votre nouveau site web
> Tout est automatiquement configuré. Pour le moment, le https montrera une erreur de configuration. En janvier, nous déploierons de nouveaux certificats letsencrypt acceptant le wildcard et ce ne sera plus vrai.
![](images/forges_logicielles-8a61e5fa.png)



# Bonus track : Configuration d'une clé GPG de signature
1. Générez une clé GPG si vous n'en n'avez pas déjà une
  - https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html
2. Dans les settings ajoutez la clé GPG
3. Ajoutez la signature automatique des commit

```
florck@florck-macbook [~/Documents/Perso/Duniter/rml10/gitlab.git] % git config --local user.signingkey 5E12B18DF10138FF                                                                                - (23:23)
florck@florck-macbook [~/Documents/Perso/Duniter/rml10/gitlab.git] % git config --local commit.gpgsign true
```

![](images/forges_logicielles-7d5e7d51.png)



# Et pour aller plus loin
## Rendez-vous dans le projet duniter/gitlab pour toutes les fiches tuto détaillant notamment la connexion avec github et bien d'autres choses.
